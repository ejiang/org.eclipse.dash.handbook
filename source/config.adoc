////
 * Copyright (C) 2015,2022 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *  
 * SPDX-FileType: SOURCE
 *
 * SPDX-License-Identifier: EPL-2.0
////

:privacyPolicyUrl: https://www.eclipse.org/legal/privacy.php
:termsOfUse: https://www.eclipse.org/legal/termsofuse.php
:codeOfConductUrl: https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php
:codeOfConductMdUrl: https://raw.githubusercontent.com/eclipse/.github/master/CODE_OF_CONDUCT.md
:communicationChannelGuidelinesUrl: https://www.eclipse.org/org/documents/communication-channel-guidelines/
:socialMediaGuidelinesUrl: https://www.eclipse.org/org/documents/social_media_guidelines.php

:emoEmail: emo@eclipse-foundation.org
:ipTeamEmail: emo-ip-team@eclipse.org
:emoRecordsEmail: emo-records@eclipse-foundation.org
:webmasterEmail: webmaster@eclipse-foundation.org
:legalEmail: license@eclipse-foundation.org
:licenseEmail: license@eclipse-foundation.org
:licenseEmailUrl: mailto:{licenseEmail}
:tckEmail: tck@eclipse.org
:tckMailUrl: mailto:{tckEmail}

:proposalsForum: http://www.eclipse.org/forums/eclipse.proposals
:activityUrl: http://www.eclipse.org/projects/project_activity.php
:edpUrl: xref:edp
:efspUrl: https://www.eclipse.org/projects/efsp/
:efslUrl: https://www.eclipse.org/legal/efsl.php
:eftcklUrl: https://www.eclipse.org/legal/tck.php
:trademarkTransferUrl: http://eclipse.org/legal/Trademark_Transfer_Agreement.pdf
:trademarkGuidelinesUrl: https://eclipse.org/legal/logo_guidelines.php
:copyrightUrl: https://www.eclipse.org/legal/copyrightandlicensenotice.php
:accountUrl: https://accounts.eclipse.org/
:memberUrl: https://www.eclipse.org/membership/
:wgUrl: https://www.eclipse.org/org/workinggroups/
:approvedLicensesUrl: https://www.eclipse.org/legal/licenses.php#approved
:proprietaryToolsUrl: https://www.eclipse.org/org/documents/Eclipse_Using_Proprietary_Tools_Final.php
:clearlyDefinedUrl: http://clearlydefined.io/
:licenseToolUrl: https://github.com/eclipse/dash-licenses
:licenseToolLink: {licenseToolUrl}[Eclipse Dash License Tool]

:webmasterEmailLink: mailto:{webmasterEmail}[Eclipse Webmaster]
:ipTeamEmailLink: mailto:{ipTeamEmail}[EMO IP Team]
:emoEmailLink: mailto:{emoEmail}[EMO]
:emoRecordsEmailLink: mailto:{emoRecordsEmail}[EMO Records Team]
:trademarkGuidelinesLink: {trademarkGuidelinesUrl}[Eclipse Foundation Trademark Usage Guidelines]

:developerPortalUrl: http://portal.eclipse.org
:bylawsUrl: https://www.eclipse.org/org/documents/eclipse_foundation-bylaws.pdf
:committerGuidelinesUrl: http://www.eclipse.org/legal/committerguidelines.php
:legalDocumentationUrl: http://www.eclipse.org/legal/guidetolegaldoc.php
:ipThirdParty: http://www.eclipse.org/org/documents/Eclipse_Policy_and_Procedure_for_3rd_Party_Dependencies_Final.pdf
:ipPolicyUrl: http://eclipse.org/org/documents/Eclipse_IP_Policy.pdf
:hostedServicesPrivacyPolicyUrl: https://www.eclipse.org/org/documents/eclipse-foundation-hosted-services-privacy-and-acceptable-usage-policy.pdf
:antitrustPolicyUrl: https://www.eclipse.org/org/documents/Eclipse_Antitrust_Policy.pdf
:mccaUrl: http://www.eclipse.org/legal/committer_process/EclipseMemberCommitterAgreementFinal.pdf
:icaUrl: http://www.eclipse.org/legal/committer_process/EclipseIndividualCommitterAgreementFinal.pdf
:ipzillaUrl: https://dev.eclipse.org/ipzilla

:cbiUrl: http://wiki.eclipse.org/CBI
:emoApprovalsUrl: https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/new?issue
:ecaUrl: https://www.eclipse.org/legal/ECA.php
:ecaSignUrl: https://dev.eclipse.org/site_login/myaccount.php#open_tab_cla
:dcoUrl: https://www.eclipse.org/legal/DCO.php
:incubationListUrl: https://dev.eclipse.org/mailman/listinfo/incubation
:incubationBrandingUrl: http://wiki.eclipse.org/Development_Resources/HOWTO/Conforming_Incubation_Branding
:membershipAgreementUrl: https://www.eclipse.org/org/documents/eclipse_membership_agreement.pdf

:jarSigningUrl: https://wiki.eclipse.org/IT_Infrastructure_Doc#Sign_my_plugins.2FZIP_files.3F
:downloadsUrl: https://wiki.eclipse.org/IT_Infrastructure_Doc#Downloads
:versionNumberingUrl: https://wiki.eclipse.org/Version_Numbering

:helpdeskUrl: https://gitlab.eclipse.org/eclipsefdn/helpdesk

:epl20Url: http://www.eclipse.org/legal/epl-2.0
:suaUrl: http://www.eclipse.org/legal/epl/notice.php
:suaHtmlUrl: http://www.eclipse.org/legal/epl/notice.html
:suaTxtUrl: http://www.eclipse.org/legal/epl/notice.txt
:suaPropertiesUrl: http://www.eclipse.org/legal/epl/feature.properties.txt

:securityPolicyUrl: https://www.eclipse.org/security/policy.php
:securityTeamEmail: security@eclipse-foundation.org
:vulnerabilityReportUrl: https://gitlab.eclipse.org/security/vulnerability-reports/-/issues/new?issuable_template=new_vulnerability
:knownVulnerabilitiesUrl: https://www.eclipse.org/security/known.php
:cveRequestUrl: https://gitlab.eclipse.org/security/cve-assignement/-/issues/new?issuable_template=cve
:cveUrl: https://cve.mitre.org/
:cweUrl: https://cwe.mitre.org/

:emoGitUrl: https://gitlab.eclipse.org/eclipsefdn/emo-team/emo
:iplabUrl: https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab
:iplabWiki: {iplabUrl}/-/wikis/home
:iplabProjectTemplate: {iplabUrl}/-/issues/new?issuable_template=vet-project
:iplabThirdPartyTemplate: {iplabUrl}/-/issues/new?issuable_template=vet-third-party

:githubSecurityAdvisoryInfoUrl: https://docs.github.com/en/code-security/security-advisories/about-github-security-advisories

:edpLink: {edpUrl}[Eclipse Foundation Development Process]
:efslLink: {efslUrl}[Eclipse Foundation Specification License]
:eftcklLink: {eftcklUrl}[Eclipse Foundation TCK License]
:ecaLink: {ecaUrl}[Eclipse Contributor Agreement]
:ipDueDiligenceLink: <<ip, Eclipse Foundation Intellectual Property Due Diligence Process>>

// Marketing
:planetEclipseUrl: https://planeteclipse.org/planet
:marketingEmail: mailto:marketing@eclipse.org
:marketingServicesUrl: https://www.eclipse.org/org/services/marketing
:eventsUrl: https://events.eclipse.org/
:newsroomUrl: https://newsroom.eclipse.org/

:adoptersProgrammeUrl: https://gitlab.eclipse.org/eclipsefdn/it/api/eclipsefdn-project-adopters
:adoptersProgrammeAdopters: {adoptersProgrammeUrl}#project-adopters
:adoptersProgrammeApi: {adoptersProgrammeUrl}#javascript-plugin

:clearlyDefinedMinimumScore: 60

:euro: &#x20AC;

:linkcss:
:stylesdir: ./resources
:scriptsdir: ./resources

:icons: font

:doctype: book
:mkdirs: true
:sectlinks:
:sectanchors:
:figure-caption!:

:experimental:
