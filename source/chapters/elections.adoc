////
 * Copyright (C) 2015,2022 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-License-Identifier: EPL-2.0
////

[[elections]]
== Elections

xref:#roles[Roles] in a project are assigned based on merit demonstrated in a publicly-accessible forum, in the form of an election. Elections start with a nomination that contains a statement of merit. The nature of a statement of merit varies widely, but is generally expected to to concisely state the impact that the nominee has had on the project.

[NOTE]
====
Employment status has no bearing at whether or not somebody can participate in an open source project at {forgeName}. Employment does not, for example, guarantee committer status; committer status must be earned by everybody.
====

[[elections-committer]]
=== Committer Elections

Contributors who have the trust of the project's committers can, through election, be promoted to _committer status_ for that project. The breadth of a committer's influence corresponds to the breadth of their contribution. A development team's contributors and committers may (and should) come from a diverse set of organisations. A committer gains voting rights allowing them to affect the future of the project. Becoming a committer is a privilege that is earned by contributing and showing discipline and good judgement. It is a responsibility that should be neither given nor taken lightly, nor is it a right based on employment by an Eclipse Foundation member company or any company employing existing committers.

Being a {forgeName} committer is more than just having write-access to the project resources: there are specific IP due diligence and record keeping activities that committers _must_ follow. New committers must ensure that they are familiar with the {committerGuidelinesUrl}[Committer Due Diligence Guidelines].

New committers should be encouraged to join the {incubationListUrl}[Incubation Mailing List]; this list is a good place to ask questions about process, services available, and other aspects of working as a committer.

[[elections-requirements]]
==== What are the Requirements?

There are only three requirements around nominating and electing new committers (note that there are additional <<paperwork, committer paperwork>> requirements for the new committer):

* Define trust;
* Maintain vendor/employer neutrality; and
* Operate transparently (public and archived election).

Each project is entitled to define how it evaluates "[people] who have the trust of the Project's Committers ... [through] contributing and showing discipline and good judgement". This definition must be consistent with the top-level project charter, and must be transparent and public document on the project's website. It is extremely important to publish these criteria to avoid any issues around cliques or "the in-crowd" preventing others from joining a project. 

There must not be any hint of "we (company W) hired person X to work on project Y thus person X should elected a committer". Committer status is independent of employment; there are well-supported mechanisms for contributors without commit-rights and thus committer status is not required for a team member to be effective. Additionally, the team will want to make sure that they have confidence in the candidate irrespective of employment and management because the committer status will continue even after moves to another job.

The nomination and election process for a new committer is for more than just the project team - it is also for the entire {forgeName} community, current and future. The larger community uses the artifacts of elections as (one of many pieces of) evidence about the maturity of the project team, and thus quality of the frameworks.

[NOTE]
====
Nominations such as "we all know Bob, vote for him" may work within the team, but actually harm the project's reputation in the larger {forgeName} community: that larger community does not know Bob and does not understand why the project team _trusts_ him with the source code.
====

[[elections-nomination]]
==== What Should a Nomination Look Like?

A committer nomination should explain the candidate's contributions to the project and thus why they should be elected as a committer. Cite the issues they have fixed via patches; cite the community forum postings they have answered; cite the xref:resources-dev-list[dev-list] design discussions to which they have contributed; etc. In all cases, provide URLs to source material.

[[elections-how]]
==== How does an Election work?

Use the menu:Committer Tools[Nominate a Committer] link on the corresponding <<pmi-project-page,project page>> and follow the workflow to start a committer election.

.The New Committer Nomination Form
image::images/committer-election2.png[]

Project committers will be notified to participate in the election via the project's xref:resources-dev-list[dev-list].

[NOTE]
====
Your project must have a _dev-list_ specified in the project's <<pmi,metadata>> and existing project team members must be subscribed to the list.
====

An election starts with a nomination by an existing committer.

[graphviz, images/elections-overview, svg]
.An overview of the Election Process
----
digraph {
	// Graph properties
	bgcolor=transparent
	rank=same
	rankdir=TB
	
	// Decisions
	node [shape=diamond;style=filled;fillcolor=white;fontsize=10];
	time [label="", group=g1];
	approval [label="PMC\nAudits", group=g1];
	
	// Notes
	node [shape=plaintext;fillcolor=transparent;fontsize=10]
	oneweek [label="One week passes or\nall committers\nhave voted"];
	
	node [shape=box;style=filled;fillcolor=white;fontsize=12];
	start [label="Nominate", group=g1];
	ongoing [label="Vote", group=g1];
	timedout [label="Timed out"];
	complete [label="Complete", group=g1];
	approved [label="Approved"];
	vetoed [label="Vetoed"];
	rejected[label="Rejected"];
	
	{rank=same timedout, approved, vetoed, rejected}
	
	edge [fontsize=10];
	start->ongoing
	ongoing->ongoing [label="Committers Vote"];
	ongoing->time
	time->timedout [xlabel="Not\nenough\nvotes"]
	time->complete
	time->rejected [xlabel="Vetoed\nby\ncommitter"]
	complete->approval
	approval->approved [xlabel="Yes"]
	approval->vetoed [xlabel="No"]
	
	edge [color=grey]
	oneweek->time
}
----

Only project committers may nominate a new committer or vote in a committer election. To be successful, the election must receive a minimum of three positive `pass:[+1]` votes. Any committer can veto the election by casting a `pass:[-1]` vote, or indicate their participation in the process but with no opinion with a `pass:[+0]` vote. For projects with three or fewer committers all committers must vote. Committer elections run for one week, but will end prematurely if all project committers vote `pass:[+1]`.

[TIP]
====
Committers can change their vote while the election is in progress. Committers may post questions about the committer or ask for clarification of the merit statement in the project's xref:resources-dev-list[dev-list], and change their vote based on what they discover. The merit statement itself cannot be changed after the election has started.
====

Following a successful committer vote, the project's xref:roles-pmc[Project Management Committee] (PMC) will xref:roles-pmc-elections[review the election results] and then either approve or veto the election. An election may be vetoed, for example, if the PMC feels that the merit statement is not strong enough.

The <<paperwork, paperwork>> process will automatically be initiated following PMC approval of an election. 

[[elections-pl]]
=== Project Lead Elections

Similar to a committer election, a project lead election starts with a statement of merit. The merit statement should, rather than focus on specific code contributions, focus instead on the leadership qualities expressed by the individual.

.Project Lead merit statement
====
Robert Smith has been part of the Eclipse Dash development since before the initial contribution. He played an important role ever since, as he is one of the key developers. With regards to the number of commits Robert is currently the top committer of Eclipse Dash:

++https://github.com/eclipse/dash-licenses/commits?author=RobertSmith++

Apart from that Robert took care of the project page and the build. He also handed the governance aspects of the 0.6 release. 

Finally I would like to mention a blog post he did at thecure.com: 

++https://thecure.com/dash++
====

Project leads are normally also committers. A project may have more than one project lead (so-called _co-leads_).

Use the menu:Committer Tools[Nominate a Project Lead] link on the corresponding <<pmi-project-page,project page>> to start a project lead election.

Only project committers can nominate a new project lead or vote in a project lead election. To be successful, a project lead election must receive a minimum of three positive `pass:[+1]` votes. Any committer can veto the election by casting a `pass:[-1]` vote. For projects with three or fewer committers all committers must vote. Committer elections run for one week, but will end prematurely if all project committers vote `pass:[+1]`.

Following a successful committer vote, the project's PMC will review the election results and then either approve or veto the election.  A PMC-approved election will be referred to the EMO/ED as a recommendation for appointment. The final decision rests with the EMO/ED.

[[elections-pmc-member]]
=== PMC Member Elections

xref:#roles-pmc[Project Management Committee] (PMC) members are elected by the existing PMC leads and members, and approved by the EMO/ED. The criteria by which a PMC _Member_ is selected varies by PMC. Some PMCs are set up to have a representative from each of the projects in the top-level project. Other PMCs are more exclusive and require a demonstration of merit.

In all cases, the PMC Lead makes a recommendation to the EMO/ED to appoint a PMC Member. The final decision rests with the EMO/ED.

[TIP]
====
No specific infrastructure is exists for PMC Member elections. PMCs must use their PMC mailing list to nominate and elect new members, and report the results to the {emoEmailLink}. The EMO will get approval from the EMO/ED and make the appointment.
====

[[elections-pmc-lead]]
=== PMC Lead Appointments

xref:#roles-pmc[Project Management Committee] (PMC) _Leads_ are not elected. They are vetted by the EMO, approved by the Eclipse Board of Directors, and appointed by the EMO/ED.

[[elections-retire-cm]]
=== Committer Retirement

A committer can retire themselves from a project. Retirement cannot be undone: to rejoin a project as a committer after being retired, a contributor must again be <<elections-committer,nominated and elected>> into the role.

[TIP]
====
To retire from a project, navigate to the <<pmi-project-page, project page>>, open the _Who's Involved_ tab, and click on your image in the committer list. Then, in the "Retire Committer Status" box on the on the project role page, check the checkbox and click the btn:[Submit] button.

image::images/elections_retire_self.png[]
====

A project lead may also retire a committer. A project lead might, for example, retire a committer that has been inactive for an extended period of time (e.g., longer than a year). Before retiring a committer, the project's community should be informed of the change and the committer must be given a chance to defend retaining their status via the project's xref:resources-dev-list[dev-list]. 

Retired committers are listed as _Historic Committers_ on a project's _Who's Involved_ page. To restore an historic committer to committer status, they must be <<elections-committer,re-elected>> into the role.

[NOTE]
====
A common practice is for the project lead to use the project's xref:resources-dev-list[dev-list] to announce generically that they're going to retire committers that have been inactive for some designated period of time and give the committers a chance to speak up before a deadline.
====

There is no means by which a committer can be forced out of a project if they have not done anything wrong (there is no notion of "voting a committer out" of a project). Leaving the company that sponsors the project, for example, is not a valid reason for retirement. On the other hand, that a committer has changed jobs or roles and can no longer continue to contribute the project is a completely valid reason for retirement.

[[elections-retire-pl]]
=== Project Lead Retirement

A project lead can retire themselves from a project. Prior to retiring, a project lead should work with the project team to ensure the continuity of the role (e.g., in the case where a project's sole project lead plans retirement, they should engage with the team to identify and <<elections-pl,elect>> a replacement). In the spirit of open source transparency, a retiring project lead should announce their retirement on the project's xref:resources-dev-list[dev-list].

[TIP]
====
To retire from a project, navigate to the <<pmi-project-page, project page>>, open the _Who's Involved_ tab, and click on your image in the project lead list. Then, in the "Retire Project Lead Status" box on the on the project role page, check the checkbox and click the btn:[Submit] button.

image::images/elections_retire_pl.png[]
====

[[elections-retire-other]]
=== Retiring Other Roles

Connect with the PMC or {emoEmailLink} for assistance.

=== Troubleshooting

In the case where the project is dysfunctional (e.g., there is no active project lead and an insufficient number of active committers to offset inactive committers), options are available to the PMC to add and/or remove committers. 

[TIP]
====
If you believe that a project is in a dysfunctional state, contact the PMC via their mailing list to request assistance.
====

[[elections-faq]]
=== Frequently Asked Questions

[qanda]
Do we really need to do this? ::

Yes.

Can I still be a committer if I change employers? ::

Yes. Your status as a committer is not tied to your employment status. You may, however, require <<paperwork>> from your new employer (or if you become self-employed). If you change employers, please contact the mailto:{emoRecordsEmail}[EMO Records Team] for help regarding paperwork requirements.
	
What happens if committers don't vote? ::

If a project has three or fewer committers, then all committers must vote. If even one out of the three does not vote, then the election will end in failure. If the non-voting committer is also not active, then they can, perhaps, be <<elections-retire-cm,retired>> by the project lead.

Can we change a merit statement? ::

The election infrastructure doesn't permit changing a merit statement after an election has begun (the principle being that changing the question after you start a vote is considered bad form).
+
In cases where the merit statement is omitted or considered inadequate, or committers have additional questions about a candidate, it's completely reasonable to discuss it on the mailing list, and for committers to then use that discussion as the basis for their vote. Committers who have already voted, can return to the election record and change their vote.
+
Committers can also add information via the comment that accompanies their vote on an election.

Can a committer change their vote on an election? ::

Yes. A committer can change their vote while an election is still in progress.

How do we transfer committers from one project to another? ::

Short answer: you don't
+
We have no concept of transferring committers. if committers need to move from one project to another, then they must be elected as committers to the new project and retire themselves from the old one.

Can a project lead retire a _disruptive_ committer? ::

The EDP allows a project lead to retire a disruptive committer. This is basically the "nuclear" option and so should be used sparingly; we have only retired a few committers in this manner. Should a project lead decide to take this step, they should be prepared to provide evidence to back up the claim of disruption. The project lead should work with the PMC and EMO to determine how (or if) that evidence is tracked and disseminated.