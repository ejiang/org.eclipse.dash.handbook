////
 * Copyright (C) 2023 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-License-Identifier: EPL-2.0
////

[#rules]
= The Rules

Summary of "the rules" based on the fundamental principles defined in the Eclipse Foundation Development Process

For many open source organisations, open means the same thing as _transparent_: open as in _open_ book. At the Eclipse Foundation, we regard being _transparent_ as the practice of making sure that the community can see and understand what the project is doing; and being _open_ as the act of giving up absolute control and welcoming the community to participate as an equal player on a _level playing field_ (i.e. being _open_ to participation by the others).
+
At the Eclipse Foundation, we take the _open_ part of open source very seriously. It’s codified in the Open Source Rules of Engagement found in the [Eclipse Foundation Development Process](https://www.eclipse.org/projects/dev_process/).
+
Everybody needs to play by the same set of rules. A level playing field doesn't necessarily mean that a project team needs to accept every single contribution that comes their way. Rather, it means that the project team needs to have a set of rules by which _everybody_ participates; and these rules can’t include things like for whom the contributor works.
+
Contribution rules can require that contributions fall within the project’s scope and current release plan. The rules can require that all code contributions be accompanied by unit tests and documentation; or that contributions implement a solution for an issue that’s been discussed by the project team in their issue tracker. Some sort of quality bar is a reasonable part of any set of contribution rules.
+
For most open source projects, these contribution rules aren't formally captured. However, most of the rules that I've listed so far collectively form a pretty reasonable default set of participation rules. A quality bar is (obviously) hard to quantify, but for many project teams it’s enough that any committer feels that the contribution should be accepted (some projects require that two committers _sign off_ on a contribution before it can be accepted).
+
Note that it’s also perfectly reasonable for a project team to require that significant contributions come with a promise of continued investment in the form of the contributor becoming a member of the project team.
+
Regardless of the rules that define the level playing field for any particular project, any content destined for the project’s code base should have some _public record of contribution_. Otherwise, the project would be operating (at least in part) hidden from community involvement and so counter to the open source rules of engagement. That public record can take the form of a Gerrit review, merged GitHub pull request, GitLab merge request, or (if you’re in a pinch) an attachment on a Bugzilla or GitHub Issue record.

Eclipse Projects are also required to implement the Eclipse Foundation's IP Policy. Tracking intellectual property grants and ensuring that all contributors and committers are covered by agreements that

[qanda]

Committers must work day-to-day in the repositories assigned to the {forgeName} project by the Eclipse Foundation ::

Making it so that people who are outside of the development team (that is, the contributors) have the ability to access the most up-to-date version of the project code is an absolute requirement. To that end, committers must push content directly into designated open repositories. {forgeName} project committers must never stage to a private or semi-private repository and then “periodically sync” to the open repository; employing this sort of strategy raises a significant barrier for entry that makes it impossible for anybody else to participate.

Project-related issues must be tracked via issue trackers assigned to the {forgeName} project by the Eclipse Foundation ::

That is, the issue trackers associated with the project's GitLab and GitHub repositories must be used for all issues related to the open source project.

Committers must be covered by a Committer Agreement ::

Either an MCCA or an ICA. When you change employers, you may need sign a new agreement.

Contributors must sign the Eclipse Contributor Agreement (ECA) ::

Regardless of how a contribution is presented, the contributor must be listed as the author in the [Git commit record](https://www.eclipse.org/projects/handbook/#resources-commit) and must complete the [Eclipse Contributor Agreement](https://www.eclipse.org/legal/ECA.php) before any contribution can be accepted.
+
Note that committers, who must already been covered by either an MCCA or an ICA, are, by virtue of that agreement, considered to have signed the ECA.

Committers must work in a vendor neutral manner ::

Contributors must create a public history of high quality contribution before becoming a committer via committer election ::

To be considered for committer status a candidate must demonstrate _merit_. Generally, this takes the form of making high-quality contributions to a project in the form of patches or new code, but can manifest in other ways. It may make sense, for example, to nominate a committer based on a history of helping users and adopters.
+
The demonstration of merit must be something that everybody can see. That is, the demonstration of merit must be _transparent_ to the general public. Again, code contributions are an obvious example of this as the commit record is open for all. This is one of the reasons that commits must be correctly attributed (i.e., the `Author` field in a Git commit must include the author's name and credentials); another reason being that implementing an effective intellectual property regime requires it. With the tools available today, it's pretty easy to point to a pattern of contribution.
+
The entire process must be _open_. That is in the sense that it is open to all comers; as I like to say (in one form or another), that the _playing field must be level_. That means that the means by which an Eclipse project team decides to make somebody a committer must not be biased by who that individual works for, or any criteria other than the the quality of the contributions they've made to the project and their demonstration that they understand their role and responsibilities as a committer.
+
> To earn committer status, a candidate must demonstrate that they understand their responsibilities under the Eclipse Foundation Development Process as well as the project code.
+
If you believe that somebody is _ready to go_ and all this public demonstration of merit is just plain silly, remember that it's not you that you need to convince: it's that developer that you don't know who is watching your project, trying to figure out what they need to do to participate, and getting hired by your company can't be the answer.
+
If you really believe that somebody that does not have a visible record of contribution is ready to be a committer, get them to contribute a few pull requests to prove it to everybody else. If they're ready to be a committer, then they should be able to do this without much trouble (if making pull requests against your project is hard for some reason, then we should address that). If they don't have anything to contribute, then why do they need to be a committer at all?
+
I'll admit that I tend to think of committer status in practical terms: when somebody _needs_ to have the powers and responsibilities of a committer in an open source project that is operating in an open and transparent manner, demonstrating merit shouldn't be too onerous.
+
As an to this rule, the set of initial committers are specified at the time a new project is proposed and created. 

Committers must be subscribed to their project's `dev` list ::
